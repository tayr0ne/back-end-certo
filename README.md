## Olá sou tayrone martins, sou desenvolvedor full stack, atualmente sou desenvolvedor pela fundação oswaldo cruz!

Esse projeto foi desenvolvido em LARAVEL, eu retirei o arquivo .env do ignore!!
Estou utilizando um baco de dados postegres no heroku na nuvem, eu subi esse banco de dados la de forma gratuita para poder fazer o projeto.

retirei também o vendor do arquivo ignore.


para rodar o projeto digite apenas seguinte comando na raiz do projeto por qualquer terminal:

    php artisan serve

    normalmente ele sobe no https://127.0.0.1:8000


 show de bola, devido o meu curto o tempo por que ate entao estou alocado em um projeto chamado SIEF da fundacao oswaldo cruz, e um sistema universitario gigantesco em fim...


 esse sistema foi desenvolvido com as seguintes tecnologias: php7.1, HTML5, CSS3, JQuery, JavaScript e p laravel


 fiz ele 100% em MVC, totalmente dividido no padrao, utilizei as questoes de seguranca do laravel para as RESTs, estou manipulando todo o sistema pelo controle de rotas e monitorando elas pelo Controller, estou utilizando o middleware para poder permitir as heads para as permicoes de RESTs


 na parte do front-end podera observar que ele so possui um botao para inclusao de curso na tela home, o sistema de busca funciona perfeitamente, porem ele nao e case sensitivity

 como nao tive muito tempo pra desenvolver devido a demanda do projeto que estou atualmente nao fiz as questoes de cruzamento de dados, utilizei apenas uma tabela no banco

 normalmente divido as tabelas por categoria e linco elas com forekey (chame estrangeira).

 e cruzo os dados utilizando Join, leftJoin

 tem um exemplo abaixo de um filtro que contrui com os joins cruzando as informacoes 


  -------------------------------------------------------------------------------------------------      
            $historicoDocente = DB::table('DADO_BASICO') 
                   ->select('UNIDADE.und_nome as nomeunidadeDocente','OFERTA_DISCIPLINAXDOCENTE.odd_iddadobasicoperfil as tipoDocente','OFERTA_DISCIPLINAXDOCENTE.odd_atuacao as atuacaoDocente','OFERTA_DISCIPLINA.ofd_dtinicio as datainicio','OFERTA_DISCIPLINA.ofd_dttermino as datatermino','PROGRAMA.pro_nome as idprogramadocente')   
                   ->selectRaw("CASE WHEN OFERTA_DISCIPLINA.ofd_dttermino is null THEN 'Sim' 
                        ELSE 'Não' END AS 'vigencia' ")
                   ->join('DADO_BASICOXPERFIL','DADO_BASICOXPERFIL.dxp_dba_id','DADO_BASICO.dba_id')
                   ->join('OFERTA_DISCIPLINAXDOCENTE','OFERTA_DISCIPLINAXDOCENTE.odd_iddadobasicoperfil','DADO_BASICOXPERFIL.dxp_iiddadobasicoperfil')
                   ->join('OFERTA_DISCIPLINA','OFERTA_DISCIPLINA.ofd_id','OFERTA_DISCIPLINAXDOCENTE.odd_idofd')
                   ->join('PROGRAMA','PROGRAMA.pro_idprograma','DADO_BASICOXPERFIL.dxp_pro_idprograma')
                   ->join('UNIDADE','UNIDADE.und_idunidade','PROGRAMA.pro_und_idunidade')
                   ->where("DADO_BASICOXPERFIL.dxp_dba_id", $id)->distinct()->get();
        
           $historicoDiscente = DB::table('DADO_BASICO')
                   ->select('UNIDADE.und_nome as nomeunidadeDocente','PROGRAMA.pro_nome as idprogramadocente','DADO_ACADEMICO.dba_id as tipoDocente','EDICAO_CURSO.edi_inicio as datainicio','EDICAO_CURSO.edi_termino as datatermino')
                   ->selectRaw("CASE WHEN EDICAO_CURSO.edi_termino is null THEN 'Sim' 
                       ELSE 'Não' END AS 'vigencia' ")
                   ->join('DADO_ACADEMICO', 'DADO_ACADEMICO.dba_id', 'DADO_BASICO.dba_id')
                   ->join('EDICAO_CURSO', 'EDICAO_CURSO.edi_id', 'DADO_ACADEMICO.edi_id')  
                   ->join('DADO_BASICOXPERFIL','DADO_BASICOXPERFIL.dxp_dba_id','DADO_BASICO.dba_id')  
                   ->join('PROGRAMAXCURSO','PROGRAMAXCURSO.pcx_idprogramacaocurso','EDICAO_CURSO.edi_pcx_idprogramacaocurso') 
                   ->join('PROGRAMA','PROGRAMA.pro_idprograma','PROGRAMAXCURSO.pro_idprograma')
                   ->join('UNIDADE','UNIDADE.und_idunidade','PROGRAMA.pro_und_idunidade')                
                   ->where("DADO_ACADEMICO.dba_id", $id)->distinct()->get();
          
           $historicoOrientador = DB::table('DADO_BASICO')
                   ->select('UNIDADE.und_nome as nomeunidadeDocente','PROGRAMA.pro_nome as idprogramadocente','ORIENTACAO.docente_id as tipoDocente','ORIENTACAO.data_inicio as datainicio','ORIENTACAO.data_termino as datatermino')
                   ->selectRaw("CASE WHEN ORIENTACAO.data_termino is null THEN 'Sim' 
                       ELSE 'Não' END AS 'vigencia' ")
                   ->join('DADO_BASICOXPERFIL','DADO_BASICOXPERFIL.dxp_dba_id','DADO_BASICO.dba_id')
                   ->join('ORIENTACAO','ORIENTACAO.docente_id','DADO_BASICOXPERFIL.dxp_dba_id') 
                   ->join('EDICAO_CURSO','EDICAO_CURSO.edi_id','ORIENTACAO.edicao_curso_id')
                   ->join('PROGRAMAXCURSO','PROGRAMAXCURSO.pcx_idprogramacaocurso','EDICAO_CURSO.edi_pcx_idprogramacaocurso') 
                   ->join('PROGRAMA','PROGRAMA.pro_idprograma','PROGRAMAXCURSO.pro_idprograma')
                   ->join('UNIDADE','UNIDADE.und_idunidade','PROGRAMA.pro_und_idunidade')                
                   ->where("ORIENTACAO.docente_id", $id)->distinct()->get();
   
                   return array(                  
                           "Docente" => $historicoDocente,  
                           "Discente" => $historicoDiscente,  
                           "Orientador" => $historicoOrientador,
                       );
   
               }
-------------------------------------------------------------------------------------------------------> esse cruzamento utilizando Join foi feito para gerar un historico no sistema de tudo o que o discente cursou em todo o sistema e programa inscrito..




----------------------------------------------------------------------------------------------------
        return $dados_basicos = DB::table('DADO_ACADEMICO')
                ->join('DADO_BASICO', 'DADO_BASICO.dba_id', 'DADO_ACADEMICO.dba_id')
                ->join('DADO_BASICOXPERFIL', 'DADO_BASICOXPERFIL.dxp_dba_id', 'DADO_ACADEMICO.dba_id')
                ->join('PAIS', 'DADO_BASICO.dba_pais', 'pais.pai_idpais')
                ->join('EDICAO_CURSO', 'EDICAO_CURSO.edi_id', 'DADO_ACADEMICO.edi_id')
                ->join('PROGRAMAXCURSO', 'PROGRAMAXCURSO.pcx_idprogramacaocurso', 'EDICAO_CURSO.edi_pcx_idprogramacaocurso')
                ->join('CURSO', 'PROGRAMAXCURSO.cur_idcurso', 'CURSO.cur_idcurso')
                ->join('NIVEL_FORMACAO', 'NIVEL_FORMACAO.nif_id', 'PROGRAMAXCURSO.pxc_nif_id')
                ->where("dxp_per_id", $request['id_perfil'])
                ->when($request, function ( $query ) use ( $request ) {
                    if ($request['form_filtro_nome'] != '') {
                        $query->where("DADO_BASICO.dba_nome", "LIKE", "%" . $request["form_filtro_nome"] . "%");
                    }
                    if ($request['form_filtro_nome_social'] != '') {
                        $query->where("DADO_BASICO.dba_nomesocial_requerimento", "LIKE", "%" . $request["form_filtro_nome_social"] . "%");
                    }
                    if ($request['form_filtro_cpf'] != '') {
                        $query->where("DADO_BASICO.dba_cpf", Helpers::FCPF($request['form_filtro_cpf']));
                    }
                    if ($request['form_filtro_nacionalidade'] != '') {
                        $query->where("PAIS.pai_descricao", "LIKE", "%" . $request["form_filtro_nacionalidade"] . "%");
                    }
                    if ($request['form_filtro_situacao'] != '') {
                        $query->where("DADO_BASICOXPERFIL.dxp_status", $request["form_filtro_situacao"]);
                    }
                    if ($request['form_filtro_natureza'] != '') {
                        $query->where("PROGRAMAXCURSO.pxc_nat_id", $request["form_filtro_natureza"]);
                    }
                    if ($request['form_filtro_tipo'] != '') {
                        $query->where("PROGRAMAXCURSO.pxc_nif_id", $request["form_filtro_tipo"]);
                    }
                    if ($request['form_filtro_edCurdo'] != '') {
                        $query->where("EDICAO_CURSO.edi_id", $request["form_filtro_edCurdo"]);
                    }
                    if ($request['form_filtro_ACDiscente'] != '') {
                        $query->where("DADO_ACADEMICO.area_concentracao_edicurso_id", $request["form_filtro_ACDiscente"]);
                    }
                })
                ->get();
    }

    --------------------------------------------------------------------------------------------------------->aqui eu faco um filtro para o usuario achar a pessoa que deseja editar suas iformacoes 

    OBS::: quando apertar algum botao para ver as informacoes do curso aguarde alguns segundos para aparecer, essa lentidao e causada devido o banco de dados esta na novem na heroku um servidor de hospedagem










    ps::: perdoe-me pelos erros de portugues e falta de acentuacao, meu teclado nao esta legal kkkkk
    ps::: as estou manipulando o banco de dados pelo migrate do laravel


    mais muito obrigado pela oportunidade,
#
#
### Espero que goste 


# ATT, Tayrone Martins
#                   Desenvolvedor Full Stack