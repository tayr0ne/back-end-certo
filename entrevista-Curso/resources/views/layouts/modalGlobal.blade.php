{{-- Modal global para uso generico --}}
{{-- chamada --}}
<div class="modal fade" id="modalGlobal" tabindex="-1" role="dialog">
    <!-- aria-hidden="true" -->
    <div class="modal-dialog modal-lg" role="document" style=" min-width: 50%;">
        <div class="modal-content" style="/*padding: 10px 5px 0 5px;*/">
            <div class="modal-header">
                <!-- [ESC] ou | Para fechar aperte [ESC] ou [Aqui] -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity: 1;" title="Fechar Modal" data-toggle="tooltip">
                    <span id="helpBlock" class="fa fa-close" aria-hidden="true"></span>
                </button>
                <!-- tive que tirar o h4 por causa da criação de hieraquia no html - ases  - dev. FR -->
                <p class="modal-title" id="modalGlobalLabel">&zwnj;</p>
            </div>
            <div class="modal-body" id="getModalGlobalLoad" style="min-height: 20%; overflow-y:scroll;">
                <!-- no-padding -->
            </div>
        </div>
    </div>
</div>