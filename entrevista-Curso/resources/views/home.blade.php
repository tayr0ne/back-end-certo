
@extends('layouts.app')
@section('conteudo')
{{ csrf_field() }}  
@if(@$usuario == "admin")
<a type="button" href="{{'/admin'}}" class="btn btn-info">Incluir curso</a>    
@endif


<div class="clearfix">&nbsp;</div>
<h5 class="card-title btn-primary"><marquee>Projeto desenvolvido por tayrone.</marquee></h5>
<div class="container">
<div class="card bg-dark text-white">
  <img class="card-img" src="http://virginiacenter.com.br/wp-content/uploads/2016/07/Capa_FB_PromocoesdeJulho1.png" alt="Card image">
  <div class="card-img-overlay">
    <p class="card-text"></p>
    <p class="card-text"></p>
  </div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="container">
            <div class="row">
              @foreach(@$listarCursos as $row) 
                  <div class="row-sm-3">
                    <div class="card border-secondary mb-3" style="max-width: 12rem;">
                      <div class="card-header">{{@$row->nome}}</div>
                        <div class="card-body text-secondary">
                        <img class="card-img" src="https://png.pngtree.com/element_origin_min_pic/17/03/26/852f2a73cf18c96fcecd8e464c69373d.jpg" alt="Card image">
                          <h5 class="card-title">_____________</h5>
                          <p class="card-text">{{@$row->descricaoCurso}}</p>
                          <button type="button" class="btn btn-primary" onclick="modalGlobalOpen('{{ route("modalCursos.modalCursos", ["id" => @$row->id, "admin" => @$usuario])}}')">Ver</button>
                          @if(@$row->precoCurso == null) <h5 class="card-title">R$ 0</h5> @elseif(@$row->precoCurso != null) <h5 class="card-title">R$ {{$row->precoCurso}}</h5> @endif
                        </div>
                      </div>
                    </div>    
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;
              @endforeach  
            </div>
</div>
@endsection('conteudo')