@extends('layouts.app')

@section('conteudo')
<div class="container">
    <div class="clearfix">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
    <div class="container">
       
    <form method="post" action="{{'/cadastrarCurso'}}" enctype="multipart/form-data"> 
    <fieldset>
        <legend>Cadastro de Curso</legend> 
        <div class="clearfix">&nbsp;</div>
         <div class="row">              
            <div class="col-md-3">
                <div class="form-group"> 
                    <label for="nome">Nome do Curso:* </label>                   
                    <input type="text" class="form-control" id="nome" name="nome" value="" required>
                </div>
            </div> 
            <div class="col-md-3">
                <div class="form-group">  
                <label for="nome">Categoria:*</label>                  
                    <select class="form-control Categoria" name="Categoria" id="Categoria" required> 
                        <option value="">-- selecione --</option>   
                        <option value="developer"> developer </option>   
                        <option value="design"> design </option>   
                        <option value="idiomas"> idiomas </option>                                                    
                    </select>
                </div>
            </div> 
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name ="banner" id="banner" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="banner">Escolha a capa do seu curso</label>
                </div>
             </div>      
        </div>
        <div class="row">
                <div class="col-md-9">
                        <label for="nome">Será Cobrado algum valor?</label>                    
                </div>
                <div class="col-md-4">                    
                    <input type="radio"  value="1" id="enable" name="Radios"    class="enable"  onclick="VerificarRadioButton();"checked > Sim
                    <input type="radio"  value="2" id="enable" name="Radios"   class="enable" onclick="VerificarRadioButton();" > Não
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 preco">
                    <div class="form-group">
                        <label for="nome">Valor: </label>
                        <input type="text" class="form-control preco" id="preco" 
                        name="preco" value="preço" minlength="1"  maxlength="70" >
                    </div>
                </div>
            </div>
        <div class="row">  
            <div class="col-md-12">
                 <div class="form-group">
                    <p><label for="nome">Descrição do curso<small class="caracteres"></small></label></p>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control" id="observacao" name="descicao"  placeholder="Max 200 linhas" rows="8"  maxlength="100" cols="30"style="resize: none;">{{@$editar->recursos}}</textarea>
                        </div>
                    </div>    
                  </div>
            </div>
        </div>    
        <button type="submit" class="btn btn-primary">Cadastrar</button>
    <fieldset>    
    </form>  
</div>    
<script>
function VerificarRadioButton() {
    $('.enable ').on('change', function() {
        var eleValor = $('.enable:checked').val();
        if (eleValor == 1) {
            $(".preco").show();
            return false;
        }
        if (eleValor == 2) {
            $(".preco").hide();
            $(".preco").val('');
            return false;
        }
    });
    $('.enable').trigger('change');
}
</script>
@endsection('conteudo')