{{ csrf_field() }}  
<fieldset>
            <legend></legend> 
<div class="container">
<div class="card bg-dark text-white">
  <img class="card-img" src="https://png.icons8.com/metro/1600/play.png" alt="Card image">
  <div class="card-img-overlay">
  </div>
</div>

<div class="clearfix">&nbsp;</div>
<div align="center">
 <div class="card" style="width: 18rem;">
   <div class="card-body">
     <h5 class="card-title"></h5>
     <h6 class="card-subtitle mb-2 text-muted">{{$all_dados->nome}}</h6>
     <p class="card-text">{{$all_dados->descricaoCurso}}</p>
     <h5 class="card-title">R$:{{$all_dados->precoCurso}}</h5>
     <div class="clearfix">&nbsp;</div>
     <button type="button" class="btn btn-success" onclick="modalGlobalOpen('{{ route("curso.curso") }}')">ASSINAR</button>
     <button type="button" class="btn btn-danger" onclick="removerCurso('{{route("removerCurso.removerCurso")}}',{{$all_dados->id}})">Deletar</button>
   </div>
 </div>
</div>
</fieldset>