<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '','middleware' => 'cors'], function()
{
     Route::group(array('prefix' => '/', 'before' => 'auth'), function() {
     Route::get('', array('as' => 'home.index', 'uses' => 'CursosController@index'));
     Route::get('admin', function () { return view('adminCursos');});
     Route::post('cadastrarCurso', array('as' => 'admin.cursos', 'uses' => 'CursosController@cadastrarCurso'));
     Route::get('pesquisa', array('as' => 'admin.pesquisa', 'uses' => 'CursosController@pesquisa'));
     Route::get('developer', array('as' => 'admin.developer', 'uses' => 'CursosController@developer'));
     Route::get('design', array('as' => 'admin.design', 'uses' => 'CursosController@design'));
     Route::get('idiomas', array('as' => 'admin.idiomas', 'uses' => 'CursosController@idiomas'));
     Route::get('modalCursos', ['as' => 'modalCursos.modalCursos', 'uses' => 'CursosController@OpenModel']);
     Route::get('removerCurso', ['as' => 'removerCurso.removerCurso', 'uses' => 'CursosController@removerCurso']);
     Route::get('curso', ['as' => 'curso.curso', 'uses' => 'CursosController@curso']);
    });
});

