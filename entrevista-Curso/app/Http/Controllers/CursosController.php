<?php

namespace App\Http\Controllers;

use App\cursoModel;

use Illuminate\Http\Request;

class CursosController extends Controller
{
    
    public function index(){
    $usuario = "admin";    
        $listarCursos = cursoModel::all();
        if(!empty($listarCursos)){$pesquisa = 'pesquisa';}
        $count = 0;
        return view('home', compact('listarCursos','pesquisa','count','usuario'));
    }

    public function cadastrarCurso(Request $request){
        $dados = $request->all();
            $salvarCurso = new cursoModel();
            $salvarCurso->categoria      = $dados["Categoria"];
            $salvarCurso->nome           = $dados['nome']; 
            $salvarCurso->descricaoCurso = $dados['descicao'];
            $salvarCurso->precoCurso     = $dados['preco'];
            $salvarCurso->tipo           = $dados['Radios'];
            $salvarCurso->save();
            return redirect('');
        }


    public function pesquisa(Request $request){
        $dados = $request->all();
        $search = $dados['search'];
        $listarCursos = cursoModel::where('nome', "LIKE", "%" .$search. "%" )->get();
        if(!empty($listarCursos)){$pesquisa = 'pesquisa';}
        if($search == null){$listarCursos = cursoModel::all();}
        return view('home', compact('listarCursos','pesquisa'));
    }    

    public function OpenModel(Request $request){  
        $dados = $request->all();
        $all_dados = cursoModel::find($dados['id']);

        return view('modalcursos', compact('all_dados'));
    }

    public function filtroCursos(){
    
    }

    public function developer(Request $request){
        $dados = $request->all();
        $listarCursos = cursoModel::where('categoria','=','developer')->get();     
        return view('home', compact('listarCursos'));

    }

    public function design(Request $request){
        $dados = $request->all();

        $listarCursos = cursoModel::where('categoria','=','design')->get();
      
        return view('home', compact('listarCursos'));
    }

    public function idiomas(Request $request){
        $dados = $request->all();
        $usuario = "admin"; 
        $listarCursos = cursoModel::where('categoria','=','idiomas')->get();

        return view('home', compact('listarCursos','pesquisa'));
    }

    public function removerCurso(Request $request){
        $dados = $request->all();
        $deletarcurso = cursoModel::find($dados['id']);

       
        if ($deletarcurso->delete()) {
            $retorno = 'Dados excluídos com sucesso.';
        }else{
            $retorno = 'Falha ao excluir.';
        }
        return json_encode($retorno);

    }

    public function curso(){
        return view('cursos');
    }


}
