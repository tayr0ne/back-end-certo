<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cursoModel extends Model
{
    protected $fillable = [
        'id'
       ,'categoria'
       ,'nome'
       ,'descricaoCurso'
       ,'precoCurso'
       ,'tipo'
       ,'remember_token'
       ,'created_at'
       ,'update_at'
       ,'imagen'
 
     ];
     protected $table = "cursos";
     protected $primaryKey = "id";


 
}
