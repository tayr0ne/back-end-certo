/**
 * @autor Guilherme Ferro - 26/04/2018.
 * @description Manipulaçao de uma unica modal global
 *
 * @include('layount.modalGlobal')
 */

function modalGlobalOpen( url , title , effect )
{
	var title  = title || "";
	var effect = effect || "lightSpeedIn"; //expand | fadeIn | contract | wiggle | lightSpeedIn
	// evitar de fechar o modal clicando fora
	$( '#modalGlobal' ).modal({/*backdrop: 'static' , keyboard: false*/});
	$( '#modalGlobal' ).find( '.modal-title' ).text( title );
	//            $( '#modalGlobal' ).find( '.modal-body' ).html( "<div class='alert alert-success'><i class='fa fa-spinner fa-2x fa-pulse'></i> Carregando ...</div>" );
	$( '#modalGlobal' ).find( '.modal-body' ).html( '<div class="overlay"> <i class="fa fa-refresh fa-spin"></i> </div>' );
	

	$.get( url ,
		        
		function( data )
		{
			$( '#modalGlobal' ).find( '.modal-body' ).html( data )
		} );       
}

function modalGlobalClose( time )
{
	var time = time || 1;
	setTimeout( function() { $( '#modalGlobal' ).modal( 'hide' ); } , time );
}



function removerCurso(url,id) {

	bootbox.confirm({
            title: 'Remover curso',
            message: "Tem certeza que deseja excluir ?",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default'
                }
			},
			
            callback: function(result) {

                if (result)
                {
                    var dados = {
                        'id': id
                    };
                    $.ajax({
                        method: "GET",
                        url: url ,
                        dataType: 'json',
                        contentType: 'application/json',
                        crossDomain: true,
                        cache: false,
                        data: dados,
                        success: function(data) {
                            bootbox.alert(data);
                            location.replace("/");
						}
					});
				}
			}
		});
	}
	


